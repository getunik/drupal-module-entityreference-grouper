# Overview
The entity reference grouper field formatter is very similar to the regular entity reference formatter that renders the referenced entities in a configurable view mode. It adds the capability to group entities together and theme the groups individually.

## Theme Hooks
The `grouper` is the machine name of the grouper plugin, the `mode` is the grouping mode configured in the formatter's settings and the `group` is the actual group name determined by the grouping plugin.

For the formatter

* `entityreference_grouper_formatter`
* `entityreference_grouper_formatter__[grouper]`
* `entityreference_grouper_formatter__[grouper]__[mode]`
* `entityreference_grouper_formatter__[mode]`
* `entityreference_grouper_formatter__[mode]__[grouper]`

For the individual groups

* `entityreference_grouper_group`
* `entityreference_grouper_group__[group]`
* `entityreference_grouper_group__[group]__[mode]`
* `entityreference_grouper_group__[mode]`
* `entityreference_grouper_group__[mode]__[group]`

# Using the Default Grouper
Put simply, the default grouper expects a `#group` property in the render array returned by the `entity_view` function.

Implement a [`hook_entity_view`](https://api.drupal.org/api/drupal/modules!system!system.api.php/function/hook_entity_view/7) and add the following code
```
$entity->content['#group'] = $entity->type;
```

Given that you have an entity reference field somewhere with the following references
* node/42 (content type `something`)
* node/44 (content type `other`)
* node/45 (content type `other`)
* node/43 (content type `something`)

the entities will be grouped as follows
* something
  * node/42
* other
  * node/44
  * node/45
* something
  * node/43

And with the appropriate template names in your theme, you can customize the markup for the formatter as well as the individual groups per group name and/or per grouping mode.
