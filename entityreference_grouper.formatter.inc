<?php

/**
 * Implements hook_field_formatter_settings_form().
 */
function entityreference_grouper_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state)
{
	$display = $instance['display'][$view_mode];
	// This gets the actual settings
	$settings = $display['settings'];
	// Initialize the element variable
	$element = array();

	$element['grouper'] = array(
		'#type' => 'select',
		'#options' => _entityreference_grouper_get_groupers_options(),
		'#title' => t('Grouper Plugin'),
		'#default_value' => $settings['grouper'],
	);

	$element['property'] = array(
		'#type' => 'textfield',
		'#title' => 'Group Property Name',
		'#description' => 'This is only relevant if an entity is part of multiple grouping "dimensions"',
		'#default_value' => $settings['property'],
	);

	$element['mode'] = array(
		'#type' => 'textfield',
		'#title' => t('Grouping Mode'),
		'#default_value' => $settings['mode'],
	);

	$entity_info = entity_get_info($field['settings']['target_type']);
	$options = array('default' => t('Default'));
	if (!empty($entity_info['view modes']))
	{
		foreach ($entity_info['view modes'] as $view_mode => $view_mode_settings)
		{
			$options[$view_mode] = $view_mode_settings['label'];
		}
	}

	$element['view_mode'] = array(
		'#type' => 'select',
		'#options' => $options,
		'#title' => t('View mode'),
		'#default_value' => $settings['view_mode'],
		'#access' => count($options) > 1,
	);

	$element['use_content_language'] = array(
		'#type' => 'checkbox',
		'#title' => t('Use current content language'),
		'#default_value' => $settings['use_content_language'],
	);

	return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function entityreference_grouper_field_formatter_settings_summary($field, $instance, $view_mode)
{
	$display = $instance['display'][$view_mode];
	$settings = $display['settings'];

	$entity_info = entity_get_info($field['settings']['target_type']);
	$summary[] = t('Grouper Plugin: @grouper (@name)', array('@grouper' => $settings['grouper'], '@name' => $settings['property']));
	$summary[] = t('Grouping Mode: @mode', array('@mode' => $settings['mode']));
	$summary[] = t('Rendered as @mode', array('@mode' => $settings['view_mode']));
	$summary[] = !empty($settings['use_content_language']) ? t('Use current content language') : t('Use field language');

	return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_prepare_view().
 */
function entityreference_grouper_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items, $displays)
{
	entityreference_field_formatter_prepare_view($entity_type, $entities, $field, $instances, $langcode, $items, $displays);
}

function _entityreference_grouper_field_formatter_get_entity_render_array($entity_type, $entity, $field, $instance, $langcode, $item, $settings)
{
	if ($settings['use_content_language'] && !empty($GLOBALS['language_content']->language))
	{
		$langcode = $GLOBALS['language_content']->language;
	}

	$entity = clone $item['entity'];
	unset($entity->content);
	$entity_element = entity_view($field['settings']['target_type'], array($item['target_id'] => $entity), $settings['view_mode'], $langcode, FALSE);

	return $entity_element;
}

/**
 * Implements hook_field_formatter_view().
 */
function entityreference_grouper_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display)
{
	$settings = $display['settings'];
	$result = array();
	$groups = array();
	$current_group = NULL;
	$grouper = _entityreference_grouper_create_grouper($settings['grouper'], $settings['property']);

	if ($grouper === NULL)
	{
		drupal_set_message(t('Grouper plugin "@plugin" does not exist', array('@plugin' => $settings['grouper'])), 'error');
		return;
	}

	foreach ($items as $delta => $item)
	{
		if (!empty($item['access']))
		{
			$entity_element = _entityreference_grouper_field_formatter_get_entity_render_array($entity_type, $entity, $field, $instance, $langcode, $item, $settings);

			// unwrap the 'entity type' => 'id' => 'content'
			$entity_element = array_values($entity_element)[0];
			$entity_element = array_values($entity_element)[0];

			$grouper->addItem($entity_element);
		}
	}

	$groups = $grouper->getGroups();

	$result[] = array(
		'#theme' => 'entityreference_grouper_formatter',
		'#grouper' => $settings['grouper'],
		'#mode' => $settings['mode'],
		'#groups' => &$groups,
	);

	return array($result);
}
