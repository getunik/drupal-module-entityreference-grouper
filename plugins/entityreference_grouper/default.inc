<?php

$plugin = array(
	'label' => t('Default Grouper'),
	'class' => 'DefaultGrouper',
);

class DefaultGrouper
{
	private $groups;
	private $current;
	private $property;

	public function __construct($property)
	{
		$this->property = '#group-' . $property;
		$this->groups = array();
		$this->current = NULL;
	}

	public function addItem($entity_element)
	{
		$group = isset($entity_element[$this->property]) ? $entity_element[$this->property] : 'none';

		if ($this->current !== NULL && $this->current['name'] !== $group)
		{
			unset($this->current);
			$this->current = NULL;
		}

		if ($this->current === NULL)
		{
			$this->current = array(
				'name' => $group,
				'items' => array(),
			);

			$this->groups[] = &$this->current;
		}

		$this->current['items'][] = $entity_element;
	}

	public function getGroups()
	{
		return $this->groups;
	}
}
